#
# Please note not all available settings / options are set here.
# For a full list, see the wiki
#

# See https://wiki.hyprland.org/Configuring/Monitors/
monitor=eDP-1,preferred,auto,1
source=~/.config/hypr/latte.conf
exec-once = hyprctl setcursor Bibata-Modern-Ice 24
exec-once = gsettings set org.gnome.desktop.interface cursor-theme 'Bibata-Modern-Ice'
exec-once = /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
exec-once = dbus-update-activation-environment --systemd GTK_BACKEND=wayland WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=Hyprland XDG_SESSION_TYPE=wayland XDG_SESSION_DESKTOP=Hyprland MOZ_ENABLE_WAYLAND=1
exec-once = systemctl --user import-environment {,WAYLAND_}DISPLAY XDG_CURRENT_DESKTOP GTK_BACKEND MOZ_ENABLE_WAYLAND
exec-once = systemctl --user start sway-session.target
exec-once = hyprlock

# See https://wiki.hyprland.org/Configuring/Keywords/ for more

# Execute your favorite apps at launch
# exec-once = waybar & hyprpaper & firefox

# Source a file (multi-file configs)
# source = ~/.config/hypr/myColors.conf

# Set programs that you use
$terminal = footclient
$fileManager = nautilus
$menu = anyrun

# Some default env vars.
env = XCURSOR_SIZE,24
env = XCURSOR_THEME,Bibata-Modern-Ice
env = QT_QPA_PLATFORMTHEME,qt5ct # change to qt6ct if you have that

# For all categories, see https://wiki.hyprland.org/Configuring/Variables/
input {
    kb_layout = us

    follow_mouse = 1

    touchpad {
        natural_scroll = true
    }

    sensitivity = 1 # -1.0 - 1.0, 0 means no modification.
}

general {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more
    gaps_in = 5
    gaps_out = 10
    border_size = 0
    col.inactive_border = rgb($tealAlpha)
    col.active_border = rgb($mauveAlpha)

    layout = dwindle

    # Please see https://wiki.hyprland.org/Configuring/Tearing/ before you turn this on
    allow_tearing = false
}

decoration {
    rounding = 0
    blur {
        enabled = false
    }
    shadow {
        enabled = true
        range = 6
        render_power = 4
        color = rgba(444444ff)
    }
}

animations {
    enabled = false
}

gestures {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more
    workspace_swipe = on
}

misc {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more
    force_default_wallpaper = 0 # Set to 0 or 1 to disable the anime mascot wallpapers
    disable_splash_rendering = true
    disable_hyprland_logo = true
}

group {
    groupbar {
        font_family = Fira Sans
        font_size = 12
        height = 20
        col.active = rgb($mauveAlpha)
        col.inactive = rgb($tealAlpha)
        col.locked_active = rgb($rosewaterAlpha)
        col.locked_inactive = rgb($tealAlpha)
    }
}

dwindle {
    force_split = 2
    special_scale_factor = 0.95
}

# window rules
windowrulev2 = fullscreen,class:(mpv) # You'll probably like this.
windowrulev2 = workspace 1,class:(firefox) # You'll probably like this.
windowrulev2 = workspace 2,class:(thunderbird) # You'll probably like this.
windowrulev2 = workspace 10,class:(Spotify) # You'll probably like this.
windowrulev2 = float,class:(pavucontrol)
windowrulev2 = float,class:(org.telegram.desktop)
windowrulev2 = float,class:(wechat)


# See https://wiki.hyprland.org/Configuring/Keywords/ for more
$mainMod = SUPER

# Example binds, see https://wiki.hyprland.org/Configuring/Binds/ for more
bind = $mainMod, Q, killactive, 
bind = $mainMod SHIFT, Q, exit, 
bind = $mainMod SHIFT, space, togglefloating, 
bind = $mainMod SHIFT, F, fullscreen, 1
bind = $mainMod, F, fullscreen, 0
bind = $mainMod, Z, exec, pypr zoom

# programs
bind = $mainMod, return, exec, $terminal
bind = $mainMod, P, submap, programs
submap = programs
bind = , W, exec, raise --class "google-chrome" --launch google-chrome-stable
bind = , W, submap, reset
bind = , E, exec, raise --class "thunderbird" --launch thunderbird
bind = , E, submap, reset
bind = , F, exec, nautilus
bind = , F, submap, reset
bind = , M, exec, raise --class "Spotify" --launch "flatpak run com.spotify.Client"
bind = , M, submap, reset
bind = , escape, submap, reset
bind = , return, submap, reset
bind = CTRL, G, submap, reset
submap = reset

# prompts
bind = $mainMod, D, submap, prompts
submap = prompts
bind = , D, exec, anyrun
bind = , D, submap, reset
# powermenu
bind = , P, exec, wlogout -b 5 -T 400 -B 400
bind = , P, submap, reset
# video
bind = , V, exec, fd -t f -e mkv -e webm -e mp4 | anyrun --plugins libstdin.so | xargs --no-run-if-empty umpv
bind = , V, submap, reset
# files
bind = , O, exec, fd -t f -e pdf -e ps -e djvu | anyrun --plugins libstdin.so | xargs --no-run-if-empty zathura
bind = , O, submap, reset
# password
bind = , S, exec,  rbw ls | anyrun --plugins libstdin.so | xargs -I {} --no-run-if-empty rbw get {} | xargs --no-run-if-empty wl-copy
bind = , S, submap, reset
# type password
bind = SHIFT, S, exec, rbw ls | anyrun --plugins libstdin.so | xargs -I {} --no-run-if-empty rbw get {} | xargs --no-run-if-empty wtype
bind = SHIFT, S, submap, reset
# fetch client
bind = , C, exec, pypr fetch_client_menu
bind = , C, submap, reset
# master
bind = , M, exec, pypr menu
bind = , M, submap, reset
# escape
bind = , escape, submap, reset
bind = , return, submap, reset
bind = CTRL, G, submap, reset
submap = reset
# bind = $mainMod, P, pseudo, # dwindle
# bind = $mainMod, J, togglesplit, # dwindle

# Move focus with mainMod + arrow keys
bind = $mainMod, H, movefocus, l
bind = $mainMod, J, movefocus, d
bind = $mainMod, K, movefocus, u
bind = $mainMod, L, movefocus, r
# swap windows
bind = $mainMod SHIFT, H, movewindoworgroup, l
bind = $mainMod SHIFT, J, movewindoworgroup, d
bind = $mainMod SHIFT, K, movewindoworgroup, u
bind = $mainMod SHIFT, L, movewindoworgroup, r

bind = $mainMod, G, submap, group
submap = group
bind = , T, togglegroup
bind = , T, submap, reset
bind = , O, changegroupactive, f
bind = SHIFT, O, changegroupactive, b
bind = , L, lockactivegroup, toggle
bind = , L, submap, reset

bind = , escape, submap, reset
bind = , return, submap, reset
bind = CTRL, G, submap, reset
submap = reset

bind = $mainMod, W, submap, window
submap = window
bind = , P, pin, active
bind = , P, submap, reset

bind = , escape, submap, reset
bind = , return, submap, reset
bind = CTRL, G, submap, reset
submap = reset

bind = $mainMod SHIFT, MINUS, movetoworkspace, special
bind = $mainMod, MINUS, togglespecialworkspace

bindl = $mainMod, S, submap, screenshot
submap = screenshot
bindl = , S, exec, grimblast --notify --freeze save screen /home/patrick/Pictures/Screenshots/"$(date '+%Y-%m-%d-%H:%M:%S')".png
bindl = , S, submap, reset
bindl = , W, exec, grimblast --notify --freeze save active /home/patrick/Pictures/Screenshots/"$(date '+%Y-%m-%d-%H:%M:%S')".png
bindl = , W, submap, reset
bindl = , A, exec, grimblast --notify --freeze save area /home/patrick/Pictures/Screenshots/"$(date '+%Y-%m-%d-%H:%M:%S')".png
bindl = , A, submap, reset
bindl = , escape, submap, reset
bindl = , return, submap, reset
bindl = CTRL, G, submap, reset
submap = reset

# Switch workspaces with mainMod + [0-9]
bind = $mainMod, 1, workspace, 1
bind = $mainMod, 2, workspace, 2
bind = $mainMod, 3, workspace, 3
bind = $mainMod, 4, workspace, 4
bind = $mainMod, 5, workspace, 5
bind = $mainMod, 6, workspace, 6
bind = $mainMod, 7, workspace, 7
bind = $mainMod, 8, workspace, 8
bind = $mainMod, 9, workspace, 9
bind = $mainMod, 0, workspace, 10

# Move active window to a workspace with mainMod + SHIFT + [0-9]
bind = $mainMod SHIFT, 1, movetoworkspace, 1
bind = $mainMod SHIFT, 2, movetoworkspace, 2
bind = $mainMod SHIFT, 3, movetoworkspace, 3
bind = $mainMod SHIFT, 4, movetoworkspace, 4
bind = $mainMod SHIFT, 5, movetoworkspace, 5
bind = $mainMod SHIFT, 6, movetoworkspace, 6
bind = $mainMod SHIFT, 7, movetoworkspace, 7
bind = $mainMod SHIFT, 8, movetoworkspace, 8
bind = $mainMod SHIFT, 9, movetoworkspace, 9
bind = $mainMod SHIFT, 0, movetoworkspace, 10

bindl = , XF86AudioRaiseVolume, exec, pactl set-sink-volume 0 +5%
bindl = , XF86AudioLowerVolume, exec, pactl set-sink-volume 0 -5%
bindl = , XF86AudioMute, exec, pactl set-sink-mute 0 toggle

bindl = , XF86MonBrightnessUp, exec, brightnessctl set +5%
bindl = , XF86MonBrightnessDown, exec, brightnessctl set 5%-

bindl = , XF86AudioPlay, exec, playerctl play-pause

# Scroll through existing workspaces with mainMod + scroll
bind = $mainMod, mouse_down, workspace, e+1
bind = $mainMod, mouse_up, workspace, e-1

# Move/resize windows with mainMod + LMB/RMB and dragging
bindm = $mainMod, mouse:272, movewindow
bindm = $mainMod, mouse:273, resizewindow

exec-once=swww img ~/Pictures/Wallpapers/$(ls ~/Pictures/Wallpapers/ | shuf -n 1) --transition-type none
