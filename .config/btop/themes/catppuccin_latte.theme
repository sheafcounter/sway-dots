# Main background, empty for terminal default, need to be empty if you want transparent background
theme[main_bg]="#ffffff"

# Main text color
theme[main_fg]="#444444"

# Title color for boxes
theme[title]="#444444"

# Highlight color for keyboard shortcuts
theme[hi_fg]="#017bca"

# Background color of selected item in processes box
theme[selected_bg]="#bbbbbb"

# Foreground color of selected item in processes box
theme[selected_fg]="#017bca"

# Color of inactive/disabled text
theme[inactive_fg]="#888888"

# Color of text appearing on top of graphs, i.e uptime and current network graph scaling
theme[graph_text]="#cc7983"

# Background color of the percentage meters
theme[meter_bg]="#bbbbbb"

# Misc colors for processes box including mini cpu graphs, details memory graph and details status text
theme[proc_misc]="#cc7983"

# CPU, Memory, Network, Proc box outline colors
theme[cpu_box]="#a65fd5" #Mauve
theme[mem_box]="#1a8e32" #Green
theme[net_box]="#db3e68" #Maroon
theme[proc_box]="#017bca" #Blue

# Box divider line and small boxes line color
theme[div_line]="#999999"

# Temperature graph color (Green -> Yellow -> Red)
theme[temp_start]="#1a8e32"
theme[temp_mid]="#bc8705"
theme[temp_end]="#b7242f"

# CPU graph colors (Teal -> Lavender)
theme[cpu_start]="#00a390"
theme[cpu_mid]="#aaaaaa"
theme[cpu_end]="#8584f7"

# Mem/Disk free meter (Mauve -> Lavender -> Blue)
theme[free_start]="#a65fd5"
theme[free_mid]="#8584f7"
theme[free_end]="#017bca"

# Mem/Disk cached meter (Sapphire -> Lavender)
theme[cached_start]="#aaaaaa"
theme[cached_mid]="#017bca"
theme[cached_end]="#8584f7"

# Mem/Disk available meter (Peach -> Red)
theme[available_start]="#e46f2a"
theme[available_mid]="#db3e68"
theme[available_end]="#b7242f"


# Mem/Disk used meter (Green -> Sky)
theme[used_start]="#1a8e32"
theme[used_mid]="#00a390"
theme[used_end]="#089ec0"

# Download graph colors (Peach -> Red)
theme[download_start]="#e46f2a"
theme[download_mid]="#db3e68"
theme[download_end]="#b7242f"

# Upload graph colors (Green -> Sky)
theme[upload_start]="#1a8e32"
theme[upload_mid]="#00a390"
theme[upload_end]="#089ec0"

# Process box color gradient for threads, mem and cpu usage (Sapphire -> Lavender-> Mauve)
theme[process_start]="#aaaaaa"
theme[process_mid]="#8584f7"
theme[process_end]="#a65fd5"
