-- setup for twilight

require('twilight').setup {
    dimming = {
        alpha = 0.3,
        color = {"#575279", "#faf4ed"}
    }
}
