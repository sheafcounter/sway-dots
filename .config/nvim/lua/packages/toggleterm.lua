require('toggleterm').setup({
    size = 20,
    open_mapping = [[<c-\>]],
    hide_numbers = true,
    shade_terminals = false,
    insert_mappings = true,
    terminal_mappings = true,
    direction = 'float',
    float_opts = {
        border = 'curved',
        title_pos = 'center'
    }
})
