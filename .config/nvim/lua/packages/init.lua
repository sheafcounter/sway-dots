-----------------------
-- packages/lazy.lua --
-----------------------

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

local plugins = {
        -- comfort
        { -- fuzzy file picker
            'nvim-telescope/telescope.nvim',
            event = "VeryLazy",
            dependencies = {
                {'nvim-lua/plenary.nvim'},
                {'nvim-lua/popup.nvim'},
                {'nvim-telescope/telescope-ui-select.nvim'},
                {'nvim-telescope/telescope-file-browser.nvim'},
                {'nvim-telescope/telescope-bibtex.nvim'}
            },
            config = function ()
                require('packages.telescope')
            end
        },
        { -- which-key, like in doom emacs
            'folke/which-key.nvim',
            event = "VeryLazy",
            -- keys = { "<leader>" },
            config = function()
                require('which-key').setup()
                require('bindings')
            end
        },
        { -- zen mode
            'Pocco81/TrueZen.nvim',
            lazy = true,
            event = "BufReadPost",
            -- keys = { "<leader>z" },
             dependencies = {{ -- dimmer using treesitter to be smart
                'folke/twilight.nvim',
                config = function()
                    require('packages.twilight')
                end
            }},
            config = function()
                require('packages.truezen')
            end
        },

        -- programming utilities
        { -- native lsp
            'neovim/nvim-lspconfig',
            dependencies = { 'onsails/lspkind.nvim' },
            lazy = true,
            after = 'nvim-cmp',
            ft = {'tex', 'lua', 'bib', 'sh', 'zsh', 'bash', 'haskell', 'html', 'css', 'nix' },
            config = function()
                require('lsp')
            end
        },
        { -- enhance lsp
            'nvimdev/lspsaga.nvim',
            event = 'LspAttach',
            config = function ()
                require('lspsaga').setup()
            end
        },
        { -- completion
            'hrsh7th/nvim-cmp',
            lazy = true,
            event = "BufReadPre",
            -- ft = {'tex', 'lua', 'bib', 'sh', 'zsh', 'bash', 'haskell' },
            dependencies = {
                { 'hrsh7th/cmp-nvim-lsp', after = 'nvim-cmp' },
                { 'hrsh7th/cmp-buffer', after = 'nvim-cmp' },
                { 'hrsh7th/cmp-path', after = 'nvim-cmp' },
                { 'hrsh7th/cmp-cmdline', after = 'nvim-cmp' },
                { 'hrsh7th/cmp-omni', after = 'nvim-cmp' },
            },
            config = function()
                require('packages.cmp')
            end
        },
        { -- treesitter (highlight, incremental selection)
            'nvim-treesitter/nvim-treesitter',
            lazy = true,
            build = ':TSUpdate',
            ft = {'bash', 'lua', 'haskell', 'sh', 'zsh', 'python', 'tex', 'html', 'css', 'nix', 'hyprlang'},
            config = function()
                require('packages.treesitter')
            end
        },
        { -- git interface
            'TimUntersberger/neogit',
            lazy = true,
            event = "VeryLazy",
            -- keys = { "<leader>g" },
            dependencies = 'nvim-lua/plenary.nvim',
            config = function()
                require('neogit').setup()
            end
        },
        { -- lsp diagnostics
            'folke/trouble.nvim',
            lazy = true,
            dependencies = "nvim-tree/nvim-web-devicons",
            ft = {'tex', 'bib', 'lua', 'sh', 'zsh', 'bash', 'haskell', 'html', 'css', 'nix'},
            config = function()
                require('trouble').setup {}
            end
        },
        { -- mason
            "williamboman/mason.nvim",
            event = "VeryLazy",
            config = function ()
                require('mason').setup()
            end
        },
        { -- mason with lspconfig
            "williamboman/mason-lspconfig.nvim",
            after = "mason.nvim",
            config = function ()
                require('mason-lspconfig').setup {
                    automatic_installation = true,
                }
            end
        },
        { -- terminal plugin
            'akinsho/toggleterm.nvim',
            event = "VeryLazy",
            config = function ()
                require('packages.toggleterm')
            end,
        },

        -- typing utilities
        { -- pencil
            'preservim/vim-pencil',
            lazy = true,
            ft = {'markdown', 'mkd'},
            config = function()
                vim.cmd('PencilHard')
            end
        },
        { -- snippets in Lua
            'L3MON4D3/LuaSnip',
            version = 'v2.*',
            build = "make install_jsregexp",
            dependencies = 'honza/vim-snippets',
            config = function ()
                require('packages.luasnip')
            end
        },
        { -- automatic close brackets
            'steelsojka/pears.nvim',
            event = "BufReadPre",
            config = function ()
                require('pears').setup()
            end
        },
        {  -- easy commenting
            'b3nj5m1n/kommentary',
            event = "BufReadPre",
        },
        { -- surround
            'ur4ltz/surround.nvim',
            event = "BufReadPre",
            config = function ()
                require('surround').setup { mappings_stype = "surround" }
            end
        },
        { -- align tables (vimscript) 
            'junegunn/vim-easy-align',
            event = "BufReadPre",
        },
        { -- find nerd icons
            '2kabhishek/nerdy.nvim',
            dependencies = {
                'stevearc/dressing.nvim',
            },
            cmd = 'Nerdy'
        },

        -- sessions
        { -- session manager
            'Shatur/neovim-session-manager',
            lazy = true,
            event = "VeryLazy",
            -- keys = { "<leader>s" },
            dependencies = { 'nvim-lua/plenary.nvim' },
            config = function ()
                require('packages.sessions')
            end
        },

        -- aesthetics
        { -- best dashboard
            'goolord/alpha-nvim',
            event = "VimEnter",
            dependencies = { 'nvim-tree/nvim-web-devicons' },
            config = function ()
                require('packages.alpha')
            end
        },
        { -- colorscheme
            'catppuccin/nvim',
            name = 'catppuccin',
            lazy = true,
            config = function ()
                require('packages.catppuccin')
            end
        },
        { --bufferline
            'akinsho/bufferline.nvim',
            event = "ColorScheme",
            dependencies = {'nvim-tree/nvim-web-devicons'},
            config = function ()
                require('packages.bufferline')
            end
        },
        { -- statusline
            'nvim-lualine/lualine.nvim',
            event = "ColorScheme",
            config = function()
                require('packages.lualine')
            end
        },
        { -- ui replacement
            'folke/noice.nvim',
            event = "VeryLazy",
            dependencies = {
                'MunifTanjim/nui.nvim',
                'rcarriga/nvim-notify',
            },
            config = function ()
                require('packages.noice')
            end
        },
}

local options = {
    performance = {
        cache = { enabled = true },
        reset_packpath = true,
        rtp = {
            reset = true,
            disabled_plugins = {
                "gzip",
                "matchit",
                "matchparen",
                "netrwPlugin",
                "tarPlugin",
                "tohtml",
                "tutor",
                "zipPlugin",
            }
        },
    }
}

require('lazy').setup(plugins, options)
