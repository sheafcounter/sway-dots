-- config for telescope.nvim

require('telescope').setup {
    defaults = {
        initial_mode = 'normal',
        prompt_prefix = "   ",
        selection_caret = "  ",
        entry_prefix = "  ",
        winblend = 0,
        border = {},
    },
    extensions = {
        ["ui-select"] = {
            require('telescope.themes').get_ivy()
        },
        file_browser = {
            theme = 'dropdown',
            hijack_netrw = true
        },
        bibtex = {
            global_files = {'/home/patrick/Documents/notes/math.bib'}
        }
    }
}
require('telescope').load_extension('ui-select')
require('telescope').load_extension('file_browser')
require('telescope').load_extension('bibtex')
