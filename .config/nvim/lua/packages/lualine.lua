-- setup for lualine

require('lualine').setup {
    options = {
        theme = 'catppuccin',
        section_separators = '',
        component_separators = ''
    }
}
