-- setup for TrueZen

require ('true-zen').setup {
    modes = {
        ataraxis = {
            minimum_writing_area = {
                width = 80
            }
        }
    },
    integrations = {
        lualine = true,
        twilight = true
    }
}
