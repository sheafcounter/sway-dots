-- setup for alpha-nvim

local alpha = require('alpha')
local dash = require('alpha.themes.dashboard')

dash.section.header.val = {
    "                                                      ",
    "  ███╗   ██╗███████╗ ██████╗ ██╗   ██╗██╗███╗   ███╗  ",
    "  ████╗  ██║██╔════╝██╔═══██╗██║   ██║██║████╗ ████║  ",
    "  ██╔██╗ ██║█████╗  ██║   ██║██║   ██║██║██╔████╔██║  ",
    "  ██║╚██╗██║██╔══╝  ██║   ██║╚██╗ ██╔╝██║██║╚██╔╝██║  ",
    "  ██║ ╚████║███████╗╚██████╔╝ ╚████╔╝ ██║██║ ╚═╝ ██║  ",
    "  ╚═╝  ╚═══╝╚══════╝ ╚═════╝   ╚═══╝  ╚═╝╚═╝     ╚═╝  ",
    "                                                      ",
}

dash.section.buttons.val = {
    dash.button( 'n', '   New file', ':ene <BAR> startinsert<CR>'),
    dash.button( 'SPC s l', '   Load session', ':SessionManager load_last_session<CR>'),
    dash.button( 'SPC s L', '   Saved sessions', ':SessionManager load_session<CR>'),
    dash.button( 'SPC f f', '   Find file', ':Telescope find_files theme=ivy<CR>'),
    dash.button( "SPC f r", "   Recent files"   , ":Telescope oldfiles theme=ivy<CR>"),
    dash.button( 'SPC f t', '   File browser', ':Telescope file_browser theme=ivy<CR>'),
    dash.button( "q", "󰿅   Quit Neovim", ":qa<CR>"),
}

local posthead = {
    type = 'text',
    val = "",
    opts = {
        position = 'center',
        hl = 'Keyword'
    }
}

dash.section.footer.val = require('alpha.fortune')

local opts = {
    layout = {
        dash.section.header,
        posthead,
        { type = 'padding', val = 1},
        dash.section.buttons,
        dash.section.footer
    },
    opts = {
        margin = 5
    }
}

alpha.setup(opts)
-- disable folding
vim.cmd([[
    autocmd FileType alpha setlocal nofoldenable
]])

vim.api.nvim_create_autocmd("User", {
  pattern = "LazyVimStarted",
  callback = function()
    local stats = require("lazy").stats()
    local ms = (math.floor(stats.startuptime * 100 + 0.5) / 100)
    posthead.val = "󱐋 Neovim loaded " .. stats.count .. " plugins in " .. ms .. "ms"
    pcall(vim.cmd.AlphaRedraw)
  end,
})
