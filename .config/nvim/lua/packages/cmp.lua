-- configuration for nvim-cmp

local cmp = require('cmp')
local lspkind = require('lspkind')
cmp.setup {
    formatting = {
        format = lspkind.cmp_format {
            mode = 'symbol_text',
            menu = {
                buffer = '[Buffer]',
                nvim_lsp = '[LSP]',
                ultisnips = '[UltiSnips]'
            },
        }
    },
    --[[ view = {
        entries = 'native'
    }, ]]
    window = {
        completion = cmp.config.window.bordered(),
        documentation = cmp.config.window.bordered(),
    },
    snippet = {
        expand = function(args)
            vim.fn["UltiSnips#Anon"](args.body)
        end,
    },
    mapping = {
        ['<C-n>'] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Insert }),
        ['<C-p>'] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Insert }),
        ['<Down>'] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Select }),
        ['<Up>'] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Select }),
        ['<C-Space>'] = cmp.mapping.complete(),
        ['<C-e>'] = cmp.mapping.close(),
        ['<CR>'] = cmp.mapping.confirm({
            behavior = cmp.ConfirmBehavior.Replace,
            select = false,
        })
    },
    sources = cmp.config.sources({
        {name = 'nvim_lsp'},
        {name = 'ultisnips'},
    }, {
        {name = 'buffer'},
        {name = 'omni'},
        {name = 'path'},
    }),
}
