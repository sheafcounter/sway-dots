------------------
-- settings.lua --
------------------

-- helpful shortcuts
local o   = vim.o
local g   = vim.g
local cmd = vim.cmd

-- global settings
-- o.background = "light"
o.ruler      = true
o.showmode   = false
o.fillchars  = o.fillchars .. 'vert:░'
o.tabstop    = 4
o.shiftwidth = 4
o.scrolloff  = 10
o.mouse = 'nv'
o.hidden = true
o.title = true

-- window local settings
o.number         = true
o.relativenumber = true
o.wrap           = true
o.foldmethod     = 'expr'
cmd('set foldexpr=nvim_treesitter#foldexpr()')
o.list       = false
o.linebreak  = true
o.cursorline = true

-- buffer local settings
o.expandtab = true

----------------------
-- package settings --
----------------------

-- completion
o.completeopt = 'menu,menuone,noselect'
o.shortmess   = o.shortmess .. 'c'

-- snippets
g.UltiSnipsExpandTrigger       = '<Tab>'
g.UltiSnipsJumpForwardTrigger  = '<c-b>'
g.UltiSnipsJumpBackwardTrigger = '<c-z>'

-- colorscheme
vim.cmd('colorscheme catppuccin')

-- add hyprlang filetype
vim.filetype.add({
  pattern = { [".*/hyprland%.conf"] = "hyprlang" },
})
vim.filetype.add({
  pattern = { [".*/hypridle%.conf"] = "hyprlang" },
})
vim.filetype.add({
  pattern = { [".*/hyprlock%.conf"] = "hyprlang" },
})
vim.filetype.add({
  pattern = { [".*/hyprpaper%.conf"] = "hyprlang" },
})

-- workaround because of changes to nvim runtimepath loading
-- cmd("set runtimepath+=~/.local/share/nvim/site/pack/packer/start/vim-snippets")
