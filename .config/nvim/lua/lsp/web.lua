-- web.lua --
-- css and html language servers --

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true

require('lspconfig').cssls.setup {
    capabilities = capabilities
}

require('lspconfig').html.setup {
    capabilities = capabilities
}
