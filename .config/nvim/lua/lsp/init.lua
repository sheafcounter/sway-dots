-------------
-- lsp.lua --
-------------

-- require('lsp.haskell')
require('lsp.latex')
require('lsp.lua')
require('lsp.shell')
require('lsp.web')
require('lsp.nix')


vim.lsp.set_log_level('error')
