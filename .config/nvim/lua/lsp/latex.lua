-- LaTeX setup for lsp

local my_capabilities = require('cmp_nvim_lsp').default_capabilities(vim.lsp.protocol.make_client_capabilities())
require('lspconfig').texlab.setup{
    settings = {
        texlab = {
            build = {
                forwardSearchAfter = true,
                onSave = true,
            },
            chktex = {
                onOpenAndSave = true,
            },
        }
    },
    capabilities = my_capabilities,
}
