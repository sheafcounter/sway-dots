-- lsp haskell setup

local my_capabilities = require('cmp_nvim_lsp').default_capabilities(vim.lsp.protocol.make_client_capabilities())
require('lspconfig').hls.setup {
    single_file_support = true,
    capabilities = my_capabilities,
}

