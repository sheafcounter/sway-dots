-- shell configuration for lsp

local my_capabilities = require('cmp_nvim_lsp').default_capabilities(vim.lsp.protocol.make_client_capabilities())
require('lspconfig').bashls.setup{
    filetypes = {'sh', 'bash', 'zsh'},
    capabilities = my_capabilities,
}
