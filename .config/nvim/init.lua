--------------
-- init.lua --
--------------

vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

require('packages') -- packages
-- require('packer_compiled') -- use packer compiled code with impatient
require('settings') -- settings
-- require('bindings') -- bindings
