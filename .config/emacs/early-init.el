;;; early-init.el --- Early Initialization file for Emacs
;;; Commentary:

;; Disable package.el

;;; Code:

(setq package-enable-at-startup nil)
;; (setq use-package-ensure-function 'ignore)
;; (setq package-archives nil)

(menu-bar-mode -1) ;; disables menubar
(tool-bar-mode -1) ;; disables toolbar
(scroll-bar-mode -1) ;; disables scrollbar


;;; early-init.el ends here
