;;; init.el --- Initialization file for Emacs
;;; Commentary:

;;; Emacs with straight.el and use-package
;;; LaTeX, nix, Elisp, lua, and web

;;; Code:

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)

;; default options
(use-package emacs
  :init
  (show-paren-mode t)
  (setq read-extended-command-predicate #'command-completion-default-include-p)
  (set-face-attribute 'default nil
		:family "Iosevka"
		:height 110
		:weight 'normal)
  (set-face-attribute 'variable-pitch nil
		:family "Iosevka Aile"
		:height 110
		:weight 'normal)
  (set-face-attribute 'fixed-pitch nil
		:family "Iosevka"
		:height 110
		:weight 'normal)
  (setq completion-cycle-threshold 3)
  (setq tab-always-indent 'complete)
  (setq initial-buffer-choice (lambda () (get-buffer-create "*dashboard*")))
  (setq native-comp-async-report-warnings-errors nil)
  (setq make-backup-files nil)
  (global-visual-line-mode)
  :hook
  (text-mode-hook . visual-line-mode))
(if (daemonp)
    (add-hook 'after-make-frame-functions
	      (lambda (frame)
                (set-face-attribute 'default nil
		:family "Iosevka"
		:height 110
		:weight 'normal)
                (set-face-attribute 'variable-pitch nil
		:family "Iosevka Aile"
		:height 110
		:weight 'normal)
                (set-face-attribute 'fixed-pitch nil
		:family "Iosevka"
		:height 110
		:weight 'normal)
		)))
(use-package electric
  :init
  (electric-pair-mode +1)
  (setq electric-pair-preserve-balance nil))

;; keybindings
(use-package general
  :straight t
  :config
  (general-evil-setup)
  (general-create-definer pl/leader-keys
    :states '(normal insert visual emacs)
    :keymaps 'override
    :prefix "SPC"
    :global-prefix "M-SPC")
  (general-create-definer pl/local-leaders
    :states '(normal insert visual emacs)
    :keymaps 'override
    :prefix ","
    :global-prefix "M-,")
  (general-unbind
    "C-x C-r"
    "C-x C-z"
    "C-x C-d"
    "<mouse-2>")
  (pl/leader-keys
    "f" '(:ignore t :wk "file")
    "ft" '(dired-jump :wk "open dired")
    "fd" '(dired :wk "edit directory"))
  (pl/leader-keys
    "B" '(:ignore t :wk "bookmark")
    "Bs" '(bookmark-set :wk "set bookmark"))
  (pl/leader-keys
    "b" '(:ignore t :wk "buffer")
    "bj" '(switch-to-next-buffer :wk "switch to next buffer")
    "bk" '(switch-to-prev-buffer :wk "switch to previous buffer")
    "bd" '(kill-this-buffer :wk "kill buffer")
    "bs" '(switch-to-buffer :wk "switch to buffer"))
  (pl/leader-keys
    "h" '(:ignore t :wk "help")
    "hv" '(describe-variable :wk "describe variable")
    "hf" '(describe-function :wk "describe function"))
  (pl/leader-keys
    "SPC" '(execute-extended-command :wk "execute command")
    "TAB" '(:keymap tab-prefix-map :wk "tab")
    "p" '(:keymap project-prefix-map :wk "project")))
(use-package evil
  :straight t
  :general
  (pl/leader-keys
    "w" '(:keymap evil-window-map :wk "window"))
  :init
  (setq evil-want-C-u-scroll t)
  (setq evil-want-C-d-scroll t)
  (setq evil-split-window-below t)
  (setq evil-vsplit-window-right t)
  (setq evil-undo-system 'undo-redo)
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (evil-mode t))
(use-package evil-collection
  :straight t
  :after evil
  :config
  (evil-collection-init))
(use-package evil-surround
  :straight t
  :after evil
  :config
  (global-evil-surround-mode 1))
(use-package evil-commentary
  :straight t
  :after evil
  :config
  (evil-commentary-mode))
(use-package evil-lion
  :straight t
  :after evil
  :config
  (evil-lion-mode))
(use-package which-key
  :straight t
  :after evil
  :init (which-key-mode))

;; completion
(use-package corfu
  :straight t
  :custom
  (corfu-auto t)
  (corfu-separator ?\s)
  (corfu-quit-no-match 'separator)
  (corfu-preview-current 'insert)
  :general
  (:keymaps 'corfu-map
   :states 'insert
	    "C-n" #'corfu-next
	    "C-p" #'corfu-previous)
  :config
  (general-add-advice '(corfu--setup corfu--teardown) :after 'evil-normalize-keymaps)
  (evil-make-overriding-map corfu-map)
  :init
  (global-corfu-mode)
  (corfu-popupinfo-mode))
(use-package kind-icon
  :straight t
  :after corfu
  :custom
  (kind-icon-default-face 'corfu-default)
  :config
  (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))
(use-package svg-lib
  :straight t)
(use-package cape
  :straight t
  :init
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-elisp-block)
  (add-to-list 'completion-at-point-functions #'cape-keyword))
(use-package vertico
  :straight t
  :init
  (vertico-mode)
  (setq vertico-cycle t)
  :general
  (:keymaps 'vertico-map
	    "C-j" 'vertico-next
	    "C-k" 'vertico-previous
	    "C-f" 'vertico-exit))
(use-package savehist
  :init
  (savehist-mode))
(use-package orderless
  :straight t
  :custom
  (completion-styles '(orderless basic))
  (completion-category-defaults nil)
  (completion-category-overrides '((file (styles partial-completion)))))
(use-package marginalia
  :straight t
  :after vertico
  :custom
  (marginalia-annotators '(marginalia-annotators-heavy marginalia-annotators-light nil))
  :init
  (marginalia-mode))
(use-package all-the-icons-completion
  :straight t
  :after (marginalia all-the-icons)
  :hook (marginalia-mode . all-the-icons-completion-marginalia-setup)
  :init
  (all-the-icons-completion-mode))
(use-package consult
  :straight t
  :general
  (pl/leader-keys
    "bb" '(consult-buffer :wk "consult buffer")
    "fg" '(consult-ripgrep :wk "consult ripgrep")
    "fr" '(consult-recent-file :wk "consult ripgrep")
    "ff" '(consult-fd :wk "consult fd")
    "h" '(:ignore t :wk "history")
    "hh" '(consult-history :wk "search history")
    "Bb" '(consult-bookmark :wk "consult bookmark")))
(use-package embark
  :straight t
  :general
  (pl/leader-keys
    "." '(embark-act :wk "Embark"))
  ("C-;" 'embark-dwim)
  :init
  (setq prefix-help-command #'embark-prefix-help-command))
(use-package embark-consult
  :straight t
  :hook (embark-collect-mode . consult-preview-at-point-mode))
(use-package citar
  :straight t
  :custom
  (citar-bibliography '("~/notes/math.bib"))
  :hook
  (LaTeX-mode . citar-capf-setup)
  (org-mode . citar-capf-setup))
(use-package citar-embark
  :straight t
  :after citar embark
  :no-require
  :config (citar-embark-mode))

;; linting
(use-package flycheck
  :straight t
  :init
  (global-flycheck-mode))

;; lsp
(use-package lsp-mode
  :straight t
  :hook (LaTeX-mode . lsp-deferred)
  :hook (nix-mode . lsp-deferred)
  :hook (lua-mode . lsp-deferred)
  :hook (web-mode . lsp-deferred)
  :hook (shell-mode . lsp-deferred)
  :hook (lsp-mode . lsp-enable-which-key-integration)
  :general
  (pl/leader-keys
    "S" '(:keymap lsp-command-map :wk "LSP commands"))
  :commands (lsp lsp-deferred))
(use-package lsp-ui
  :straight t
  :after lsp-mode
  :commands lsp-ui-mode)
(use-package lsp-nix
  :straight lsp-mode
  :after (lsp-mode)
  :demand t
  :custom
  (lsp-nix-nil-formatter ["nixpkgs-fmt"]))
(use-package consult-lsp
  :straight t)

;; snippets
(use-package yasnippet
  :straight t
  :config
  (setq yas-snippet-dirs
	'("~/.config/emacs/snippets"))
  (yas-reload-all)
  (yas-global-mode 1))
(use-package aas
  :straight t
  :hook (LaTeX-mode . aas-activate-for-major-mode)
  :hook (latex-mode . aas-activate-for-major-mode)
  :config
  (aas-set-snippets 'latex-mode
		    "*b" (lambda () (interactive)
			   (yas-expand-snippet "\\textbf{$1}$0"))
		    "*i" (lambda () (interactive)
			   (yas-expand-snippet "\\textit{$1}$0"))
		    ))

;; org-mode
(use-package org
  :init
  (setq org-todo-keywords
	'((sequence "TODO(t)" "SOMEDAY(s)" "|" "DONE(d)")
	  (sequence "TO-READ(r)" "READING(R)" "|" "HAVE-READ(h)")
	  (sequence "TRY(T)" "TRYING(y)" "|" "TRIED(Y)")))
  :custom
  (org-agenda-files '("~/org/todo.org" "~/org/research.org" "~/org/teaching.org" "~/org/notes.org"))
  :general
  (pl/local-leaders
   :keymaps 'org-mode-map
   "l" '(:ignore t :wk "link")
   "ll" '(org-insert-link t :wk "insert link")
   "h" '(consult-org-heading :wk "consult heading")
   "d" '(org-todo :wk "todo")
   "i" '(:ignore t :wk "insert")
   "ih" '(org-insert-heading :wk "insert heading")
   "is" '(org-insert-subheading :wk "insert subheading"))
  (pl/leader-keys
    "o" '(:ignore t :wk "org")
    "od" '(org-todo :wk "todo")
    "oc" '(org-capture :wk "capture")
    "os" '(:ignore t :wk "time-stamp")
    "osi" '(org-time-stamp :wk "insert")
    "osa" '(org-toggle-timestamp-type :wk "toggle in/active state")
    "oa" '(org-agenda :wk "agenda")
    "ol" '(:ignore t :wk "latex")
    "olp" '(org-latex-preview :wk "preview")
    "ot" '(:ignore t :wk "toggle")
    "otp" '(org-toggle-inline-images :wk "toggle images")
    "oti" '(org-toggle-item :wk "toggle item")
    "oto" '(org-toggle-ordered-property :wk "toggle item")
    "ota" '(org-toggle-archive-tag :wk "toggle item")
    "ot;" '(org-toggle-comment :wk "toggle comment")
    "op" '(:ignore t :wk "property")
    "ops" '(org-set-property :wk "set property")
    "opn" '(org-property-next-allowed-value :wk "next value")
    "opp" '(org-property-previous-allowed-value :wk "previous value")
    "opd" '(org-delete-property :wk "delete property"))
  :hook
  (org-mode . (lambda () (electric-indent-local-mode -1)))
  :config
  (setq org-hide-emphasis-markers t)
  (setq org-directory "~/org")
  (setq org-default-notes-file "~/org/notes.org"))
(use-package org-tempo
  :after org)
(use-package org-journal
  :straight t
  :general
  (pl/leader-keys
    "j" '(:ignore t :wk "journal")
    "jn" '(org-journal-new-entry :wk "new entry"))
  :config
  (setq org-journal-file-type 'daily
	org-journal-dir "~/org/journal"))
(use-package evil-org
  :straight t
  :after org
  :hook (org-mode . (lambda () evil-org-mode))
  :config
  (require 'evil-org-agenda)
  (evil-org-agenda-set-keys))
(use-package org-modern
  :straight t
  :after org
  :config (global-org-modern-mode))

;; languages
(use-package lua-mode ;; lua
  :straight t
  :mode "\\.lua\\'"
  :interpreter "lua")
(use-package lispy ;; lisp
  :straight t
  :general
  (:keymaps 'lispy-mode-map
	    "TAB" 'indent-for-tab-command)
  :hook (emacs-lisp-mode . lispy-mode))
(use-package lispyville ;; lisp evil
  :straight t
  :hook (lispy-mode . lispyville-mode)
  :general
  (:keymaps 'lispyville-mode-map
	    "TAB" 'indent-for-tab-command)
  :config
  (lispyville-set-key-theme '(operators c-w additional)))
(use-package nix-mode ;; nix
  :straight t
  :mode "\\.nix\\'")
(use-package web-mode ;; web
  :straight t
  :mode "\\.html?\\'"
  :mode "\\.css\\'"
  :config
  (setq web-mode-enable-auto-pairing t)
  (setq web-mode-enable-css-colorization t))
(use-package latex
  :straight auctex
  :defer t
  :mode ("\\.tex\\'" . latex-mode)
  :hook
  (LaTeX-mode . turn-on-reftex)
  :init
  (add-hook 'TeX-after-compilation-finished-functions #'TeX-revert-document-buffer)
  :config
  (setq TeX-auto-save t)
  (setq TeX-parse-self t)
  (setq-default TeX-master nil)
  (setq TeX-save-query nil)
  (setq TeX-view-program-selection '((output-pdf "PDF Tools")))
  (setq TeX-view-program-list '(("PDF Tools" "TeX-pdf-tools-sync-view")))
  (setq reftex-plug-into-AUCTeX t)
  (setq TeX-source-correlate-method 'synctex)
  (setq TeX-source-correlate-start-server t)
  (TeX-source-correlate-mode t)
  :general
  (pl/leader-keys
    "l" '(:ignore t :wk "LaTeX")
    "ll" '(TeX-command-run-all :wk "compile")
    "lm" '(TeX-command-master :wk "run command")
    "lp" '(TeX-view :wk "preview")
    "lc" '(TeX-clean :wk "clean")))
(use-package evil-tex
  :straight t
  :hook (LaTeX-mode . evil-tex-mode))
(use-package auctex-latexmk
  :straight t
  :after latex
  :init
  (setq auctex-latexmk-inherit-TeX-PDF-mode t)
  :config
  (auctex-latexmk-setup))
(use-package pdf-tools
  :straight t
  :mode ("\\.pdf\\'" . pdf-tools-install)
  :defer t
  :config
  (setq mouse-wheel-follow-mouse t))

;; tools
(use-package magit
  :straight t
  :general
  (pl/leader-keys
    "g" '(:ignore t :wk "git")
    "gg" '(magit-status :wk "status")))
(use-package vterm
  :straight t)
(use-package vterm-toggle
  :straight t
  :after vterm
  :general
  (pl/leader-keys
    "t" '(:ignore t :wk "toggle")
    "tv" '(vterm-toggle :wk "toggle vterm")
    "tn" '(display-line-numbers-mode :wk "toggle line numbers")
    "tt" '(visual-line-mode :wk "toggle visual line mode"))
  :config
  (setq vterm-toggle-fullscreen-p nil)
  (setq vterm-toggle-scope 'project)
  (add-to-list 'display-buffer-alist
             '((lambda (buffer-or-name _)
                   (let ((buffer (get-buffer buffer-or-name)))
                     (with-current-buffer buffer
                       (or (equal major-mode 'vterm-mode)
                           (string-prefix-p vterm-buffer-name (buffer-name buffer))))))
                (display-buffer-reuse-window display-buffer-at-bottom)
                ;;(display-buffer-reuse-window display-buffer-in-direction)
                ;;display-buffer-in-direction/direction/dedicated is added in emacs27
                ;;(direction . bottom)
                ;;(dedicated . t) ;dedicated is supported in emacs27
                (reusable-frames . visible)
                (window-height . 0.3))))

;; appearance
(use-package mood-line
  :straight t
  :config
  (mood-line-mode)
  (setq mood-line-glyph-alist mood-line-glyphs-unicode))
(use-package all-the-icons
  :straight t)
(use-package all-the-icons-dired
  :straight t
  :hook
  (dired-mode . all-the-icons-dired-mode))
(use-package solaire-mode
  :straight t
  :config
  (solaire-global-mode +1))
(use-package hl-todo
  :straight t
  :init
  (global-hl-todo-mode))
(use-package dired-subtree
  :straight t)
(use-package dashboard
  :straight t
  :config
  (dashboard-setup-startup-hook))

(setq modus-themes-bold-constructs t
    modus-themes-italic-constructs t
    modus-themes-tabs-accented t
    modus-themes-org-blocks nil
    modus-themes-scale-headings t
    modus-themes-common-palette-overrides
    '((border-mode-line-active bg-mode-line-active)
      (border-mode-line-inactive bg-mode-line-inactive))
    modus-themes-headings
    '((1 . (rainbow 1.5))
      (2 . (rainbow 1.3))
      (3 . (rainbow 1.2))
      (t . (semilight 1.1))))
(load-theme 'modus-operandi)


;;; init.el ends here
