# Only source this once
export QT_QPA_PLATFORMTHEME="qt6ct"
export QT_QPA_PLATFORM="wayland"
export XCURSOR_PATH="/usr/share/icons"
export XCURSOR_THEME="Bibata-Modern-Ice"
export HYPRCURSOR_THEME="Bibata-Modern-Ice"
export HYPRCURSOR_SIZE=24
export MOZ_ENABLE_WAYLAND=1
export GTK_BACKEND=wayland
export BROWSER=brave
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx
export EDITOR=nvim


export XDB_DEFAULT_OPTIONS="ctrl:nocaps"
export PATH=$PATH:"$HOME/.local/bin"
export PATH=$PATH:"$HOME/bin"
export PATH=$PATH:"/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl"
export PATH=$PATH:"$HOME/.cargo/bin"
